const ffmpegPath = require("@ffmpeg-installer/ffmpeg").path;
const ffmpeg = require("fluent-ffmpeg");
const path = require("path");

ffmpeg.setFfmpegPath(ffmpegPath);

module.exports = async function convertWebMBlobToWavAndSave(source, outputDirectory = '../sound') {
    return new Promise(async (resolve, reject) => {
        const outputFileName = "output.wav";
        const outputPath = path.join(__dirname, '../sound/', outputFileName);
        ffmpeg()
            .input(source)
            .audioCodec("pcm_s16le")
            .audioChannels(1)
            .toFormat("wav")
            .on("end", () => {
                resolve(outputFileName);
            })
            .on("error", (err) => {
                reject(err);
            })
            .save(outputPath);
    });
}