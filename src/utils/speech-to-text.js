const fs = require('fs');
const sdk = require("microsoft-cognitiveservices-speech-sdk");
const speechConfig = sdk.SpeechConfig.fromSubscription("682284bdc105420da6dbd696154d89ca", "eastasia");
const path = require('path');

function fromFile() {
    const options = {
        referenceText: 'How old are you?',
        gradingSystem: "HundredMark",
        granularity: "Phoneme",
        phonemeAlphabet: "IPA"
    }

    // let pushStream = sdk.AudioInputStream.createPushStream()
    // sdk.AudioConfig.fromStreamInput()
    // fs.createReadStream("./someFile.wav").on('data', function (arrayBuffer) {
    //     pushStream.write(arrayBuffer.slice());
    // }).on('end', function () {
    //     pushStream.close();
    // });

    // let audioConfig = sdk.AudioConfig.fromStreamInput(pushStream);

    var pronunciationAssessmentConfig = sdk.PronunciationAssessmentConfig.fromJSON(JSON.stringify(options));
    const filePath = path.join(process.cwd(), "./output.wav");
    let audioConfig = sdk.AudioConfig.fromWavFileInput(fs.readFileSync(filePath));
    let speechRecognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);
    pronunciationAssessmentConfig.applyTo(speechRecognizer);

    speechRecognizer.recognizeOnceAsync(result => {
        console.log(result)
        switch (result.reason) {
            case sdk.ResultReason.RecognizedSpeech:
                console.log(`RECOGNIZED: Text=${result.text}`);
                // The pronunciation assessment result as a Speech SDK object
                var pronunciationAssessmentResult = sdk.PronunciationAssessmentResult.fromResult(result);
                console.log(`pronunciationAssessmentResult: ${pronunciationAssessmentResult}`)

                // The pronunciation assessment result as a JSON string
                var pronunciationAssessmentResultJson = result.properties.getProperty(sdk.PropertyId.SpeechServiceResponse_JsonResult);
                console.log(`pronunciationAssessmentResultJson: ${pronunciationAssessmentResultJson}`)
                break;
            case sdk.ResultReason.NoMatch:
                console.log("NOMATCH: Speech could not be recognized.");
                break;
            case sdk.ResultReason.Canceled:
                const cancellation = sdk.CancellationDetails.fromResult(result);
                console.log(`CANCELED: Reason=${cancellation.reason}`);

                if (cancellation.reason == sdk.CancellationReason.Error) {
                    console.log(`CANCELED: ErrorCode=${cancellation.ErrorCode}`);
                    console.log(`CANCELED: ErrorDetails=${cancellation.errorDetails}`);
                    console.log("CANCELED: Did you set the speech resource key and region values?");
                }
                break;
        }
        speechRecognizer.close();
    });
}
fromFile();




// const sdk = require("microsoft-cognitiveservices-speech-sdk");
// const subscriptionKey = '682284bdc105420da6dbd696154d89ca';
// const region = 'eastasia';
// const speechConfig = sdk.SpeechConfig.fromSubscription(subscriptionKey, region);

// async function blobToArrayBuffer(blob) {
//     if ('arrayBuffer' in blob) return await blob.arrayBuffer();

//     return new Promise((resolve, reject) => {
//         const reader = new FileReader();
//         reader.onload = () => resolve(reader.result);
//         reader.onerror = () => reject();
//         reader.readAsArrayBuffer(blob);
//     });
// }

// export function pronunciation(blob, text) {
//     let pushStream = sdk.AudioInputStream.createPushStream();
//     const options = {
//         referenceText: text,
//         gradingSystem: "HundredMark",
//         granularity: "Phoneme",
//         phonemeAlphabet: "IPA"
//     }

//     var pronunciationAssessmentConfig = sdk.PronunciationAssessmentConfig.fromJSON(JSON.stringify(options));

//     blobToArrayBuffer(blob).then((arrayBuffer) => {
//         pushStream.write(arrayBuffer.slice());
//     }).finally(() => {
//         pushStream.close();
//     })

//     let audioConfig = sdk.AudioConfig.fromStreamInput(pushStream);

//     var speechRecognizer = new sdk.SpeechRecognizer(speechConfig, audioConfig);
//     pronunciationAssessmentConfig.applyTo(speechRecognizer);

//     speechRecognizer.recognizeOnceAsync(result => {
//         // The pronunciation assessment result as a Speech SDK object
//         var pronunciationAssessmentResult = sdk.PronunciationAssessmentResult.fromResult(result);

//         // The pronunciation assessment result as a JSON string
//         var pronunciationAssessmentResultJson = result.properties.getProperty(sdk.PropertyId.SpeechServiceResponse_JsonResult);
//         console.log(`RECOGNIZED: Text=${result.text}`);
//         console.log(`pronunciationAssessmentResult: ${pronunciationAssessmentResult}`)
//         console.log(`pronunciationAssessmentResultJson: ${pronunciationAssessmentResultJson}`)
//         speechRecognizer.close();
//     });
// }

