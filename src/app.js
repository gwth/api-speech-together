const express = require('express');
const cors = require('cors');
const formidable = require('formidable');
const Pronunciation = require('./utils/pronunciation');
const convertWebMBlobToWavAndSave = require('./utils/convert-webm-to-wav');
require('dotenv').config()

const PORT = process.env.PORT || 4000

const app = express();

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors({
    origin: "*"
}))


app.get('/', (req, res) => {
    res.send("Heath Check")
})

app.get('/api/pronunciation', async (req, res) => {
    var reference_text = "Hello, my name is Suda. I'm from the city. I'm 25 years old. I work in an office. I like reading books and watching movies. I also enjoy going for walks in the park. It’s nice to meet you!";

    const result = await Pronunciation(reference_text)
    res.json(result)
})

app.post('/api/upload', (req, res) => {
    try {
        const form = new formidable.IncomingForm();
        form.parse(req, (err, fields, files) => {
            if (err)
                throw err;

            const { referent_text } = fields

            const webmFile = files.webmFile
            convertWebMBlobToWavAndSave(webmFile[0].filepath).then((filename) => {
                Pronunciation(referent_text[0], filename).then((result) => {
                    res.json(result)
                })
            })

        })

    } catch (error) {
        res.json({
            error: error.message
        })
    }
})

app.post('/api/uploadCN', (req, res) => {
    try {
        const form = new formidable.IncomingForm();
        form.parse(req, (err, fields, files) => {
            if (err)
                throw err;

            const { referent_text } = fields

            const webmFile = files.webmFile
            convertWebMBlobToWavAndSave(webmFile[0].filepath).then((filename) => {
                Pronunciation(referent_text[0], filename, 'zh-CN').then((result) => {
                    res.json(result)
                })
            })

        })

    } catch (error) {
        res.json({
            error: error.message
        })
    }
})



app.listen(PORT, () => console.log("Server is running on port " + PORT))